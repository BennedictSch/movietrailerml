Dieses Programm ist in der Lage, mithilfe eines Convolutinal Neural Network und eines weiteren Machine-Learning-Algorithmus eigenständig einen Trailer zu einem Film zu erstellen.
Verwendet werden der VGG16-Algorithmus (CNN), trainiert auf der ImageNet-Datenbank und eine Support-Vector-Machine, die Anhand von Features und Labels entscheidet, welche Szenen im Trailer verwendet werden.

Ein Projekt von Johanna Meyer, Jonas Christmann, Tobias Behn und Bennedict Schweimer.