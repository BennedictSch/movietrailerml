import json
import cv2
import numpy as np
import multiprocessing
import time
from scenedetect import VideoManager, SceneManager
from scenedetect.scene_manager import FrameTimecode
from scenedetect import  open_video, AdaptiveDetector
import tensorflow as tf
from PIL import Image
import sklearn
from sklearn.model_selection import train_test_split
from tqdm import tqdm

# Liste der gefundenen Indexe(Threadübergreifend)
found_indices=[]

# Szenen aus einer Videodatei extrahieren
def extract_scenes(video_path):
    video = open_video(video_path)
    scene_manager = SceneManager()
   
    scene_manager.add_detector(AdaptiveDetector())
 
    scene_manager.detect_scenes(frame_source=video)

    scene_list = scene_manager.get_scene_list()

    return scene_list

    # Fortschritt beenden
   
# Keyframes aus dem Trailer extrahieren
def extract_shot_frames_trailer(video_path, movie_scene_list):
    
    video = cv2.VideoCapture(video_path)
    shots_condensed = []
    count = 0
 
    for movie_scene in movie_scene_list:
        count+=1
        video.set(cv2.CAP_PROP_POS_FRAMES, movie_scene[0].get_frames() + 1)
        ret, shot_start_frame = video.read()
        shots_condensed.append(shot_start_frame)
          
    return shots_condensed

# Keyframes aus dem Film extrahieren
def extract_shot_frames_movie(video_path, movie_scene_list, limit):
    video = cv2.VideoCapture(video_path)
    shots_condensed = []
    scene_frames = []

    # Fortschrittsbalken erstellen
    progress_bar = tqdm(total=len(movie_scene_list), desc='Extracting shot frames')

    for movie_scene in movie_scene_list:
        video.set(cv2.CAP_PROP_POS_FRAMES, movie_scene[0].get_frames() + 1)
        ret, shot_start_frame = video.read()

        # Bestimmen der Einstellungslänge
        start = movie_scene[0].get_frames()
        end = movie_scene[1].get_frames()
        shot_length = end - start + 1

        # Anzahl der Keyframes basierend auf der Einstellungslänge festlegen
        if shot_length > limit:
            keyframe_indices = [0, shot_length // 2, shot_length - 1]
        else:
            keyframe_indices = [shot_length // 2]

        keyframes = []

        for idx in keyframe_indices:
            video.set(cv2.CAP_PROP_POS_FRAMES, start + idx)
            ret, frame = video.read()
            keyframes.append(frame)
            scene_frames.append([start,end -1])

        shots_condensed.extend(keyframes)

        # Fortschritt aktualisieren
        progress_bar.update(1)

    video.release()
    progress_bar.close()

    return shots_condensed, scene_frames


# Vergleich von Trailer-Keyframes und Movie-Keyframes
def compare_videos(shot, shot_movie, ind):
    frame_count = 0
    # Video-Capture-Objekte erstellen
    result = -1
    # SIFT-Objekt initialisieren
    sift = cv2.SIFT_create()

    # SIFT-Merkmale berechnen
    keypoints1, descriptors1 = sift.detectAndCompute(shot, None)
    keypoints2, descriptors2 = sift.detectAndCompute(shot_movie, None)

    # Überprüfen, ob Merkmalsbeschreibungen vorhanden sind
    if descriptors1 is not None and descriptors2 is not None:
        # Merkmalsbeschreibungen in den gleichen Datentyp konvertieren
        descriptors1 = descriptors1.astype(descriptors2.dtype)

        # Übereinstimmungen zwischen den Merkmalsbeschreibungen finden
        bf = cv2.BFMatcher()
        matches = bf.knnMatch(descriptors1, descriptors2, k=2)

        # Gute Übereinstimmungen auswählen (basierend auf dem Lowe's Ratio Test)
        good_matches = []
        for match in matches:
            if len(match) == 1:
                # Wenn das Tupel nur ein Element hat
                break
            else:
                # Wenn das Tupel zwei Elemente hat
                m, n = match
                if m.distance < 0.7 * n.distance:
                    good_matches.append(m)
                    cv2.destroyAllWindows()

        if len(good_matches) > 50:
           
            cv2.imshow("movie", shot)
            cv2.imshow("trailer", shot_movie)
            cv2.waitKey(5)

            result = 1
            
            found_indices.append(ind) # Hinzufügen des gefundenen Index zur gemeinsam genutzten Liste

        return result

# Vergleich der Einzelbilder aus Trailer und Film
def search_trailer_in_movie(args):
    keyframe, found_indices_local, shots_trailer = args  # Extrahieren der Argumente
    res = -1

    for i in range(len(shots_trailer)):
        if i not in found_indices_local:
            if compare_videos(shots_trailer[i], keyframe, i) == 1:
                res = 1
                found_indices_local.append(i)  # Hinzufügen des gefundenen Index zur lokalen Liste
                break
    print(np.array(found_indices_local))
    return res

# eigentliche Extraktion der Features mit VGG16
def run(movie):
    features=[]
    base_model = tf.keras.applications.vgg16.VGG16(weights = 'imagenet', include_top = True)
    frame_features = []

    # Frame Extraktion Movie
    print("Starting frame extraction.")
    scene_list_movie = extract_scenes(movie)
    shots_movie, scene_frames = extract_shot_frames_movie(movie, scene_list_movie,125)

    # Features aller Frames nacheinander mit VGG16 extrahieren 
    print("Starting feature generation.")
    for i in range(len(shots_movie)):
        img = Image.fromarray(np.uint8(shots_movie[i]))
        img= img.resize((224,224))
        x= tf.keras.preprocessing.image.img_to_array(img)
        x = x.reshape((1, x.shape[0], x.shape[1], x.shape[2]))
        x = tf.keras.applications.vgg16.preprocess_input(x)
        frame_features  = base_model.predict(x)
        features.append(frame_features.tolist())
    features = np.array(features)
    scene_frames = np.array(scene_frames)

    print("Writing json file.")
        # Speichern der Features in einer Datei
    data = {'features': features.tolist(), 'scene_frames': scene_frames.tolist()}
    filename = movie.split('.')[0]+'.json'
    with open(filename, 'w') as file:
                json.dump(data, file)

    return filename