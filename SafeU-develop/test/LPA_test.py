""" 
Unittest for `classification' package
Run in `test' folder
"""
import unittest
import numpy as np
import warnings

from safeu.classification.LPA import LPA
from safeu.Experiments import SslExperimentsWithGraph


class TestLPA(unittest.TestCase):
    def setUp(self):
        warnings.filterwarnings('ignore')
        pass

    def tearDown(self):
        pass

    def test_params(self):
        model = LPA()
        self.assertEqual(model.kernel, 'rbf')
        self.assertEqual(model.gamma, 20)
        self.assertEqual(model.n_neighbors, 7)
        self.assertEqual(model.max_iter, 30)
        self.assertEqual(model.tol, 1e-3)
        self.assertIsNone(model.n_jobs)
        model.set_params({'kernel': "Linear", 'gamma': 50,
                         'n_neighbors': 10})
        self.assertEqual(model.kernel, 'Linear')
        self.assertEqual(model.gamma, 50)
        self.assertEqual(model.n_neighbors, 10)

    def test_acc_WOGraph(self):
        '''
        '''
        configs = [
        ('LPA', LPA(), {
            'kernel': ['rbf'],
        })
        ]

        datasets = [
            ('ionosphere', None, None, None, None)
        ]

        experiments = SslExperimentsWithGraph(n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

        average_acc = np.mean(results.get('ionosphere').get('LPA'))
        self.assertEqual(average_acc > 0.5, True)

        experiments = SslExperimentsWithGraph(n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='zero_one_loss',
                                metric_large_better=False)

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

        average_acc = np.mean(results.get('ionosphere').get('LPA'))
        self.assertEqual(average_acc < 0.5, True)

    def test_acc_WGraph(self):
        '''
        '''
        configs = [
            ('LPA', LPA(), {
                'kernel': ['rbf'],
            })
        ]

        datasets = [
            ('covtype', None, None, None, None)
        ]

        experiments = SslExperimentsWithGraph(n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

        average_acc = np.mean(results.get('covtype').get('LPA'))
        self.assertEqual(average_acc > 0.4, True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestLPA))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())

    #
