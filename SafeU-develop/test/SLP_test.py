""" 
Unittest for `classification' package
Run in `test' folder
"""
import unittest
import numpy as np

from safeu.datasets.base import load_clean1
from safeu.data_quality.SLP import SLP
from safeu.Experiments import SslExperimentsWithGraph


class TestSLP(unittest.TestCase):
    def setUp(self):
        self.X, self.y = load_clean1(True)
    
    def tearDown(self):
        pass

    def test_params(self):
        model = SLP()
        self.assertEqual(model.stepSize, 0.1)
        self.assertEqual(model.T, 6)
        model.set_params({'stepSize': 0.05, 'T': 10})
        self.assertEqual(model.stepSize, 0.05)
        self.assertEqual(model.T, 10)

    def test_acc(self):
        '''
        '''
        configs = [

            ('SLP', SLP(), {
                'stepSize': [0.1],
                'T': [6]
            })
        ]

        # datasets
        datasets = [
            ('covtype', None, None, None, None)
        ]
        experiments = SslExperimentsWithGraph()

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=1 - 0.008, test_ratio=0.2, number_init=1)

        average_acc = np.mean(results.get('covtype').get('SLP'))
        print(average_acc)
        self.assertEqual(average_acc > 0.75, True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestSLP))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())

    #
