""" 
Unittest for `classification.CoTraining' package
Run in `test' folder
"""
import unittest

import numpy as np
import os

from safeu.datasets.base import load_clean1
from safeu.datasets.base import load_dataset
from safeu.datasets import data_manipulate
from safeu.classification.CoTraining import CoTraining
from safeu.Experiments import SslExperimentsWithoutGraph


class TestCoTraining(unittest.TestCase):
    def setUp(self):
        self.X, self.y = load_clean1(True)
    
    def tearDown(self):
        pass

    def test_params(self):
        model = CoTraining()
        self.assertEqual(model.pos, 1)
        self.assertEqual(model.neg, 1)
        model.set_params({'nepo': 60, 'buffer_size': 100})
        self.assertEqual(model.nepo, 60)
        self.assertEqual(model.buffer_size, 100)

    def test_acc(self):
        '''
        '''
        test_ratio = 0.2
        unlabel_ratio = 0.75
        number_init = 5
        all_class = True

        configs = [
            ('CoTraining', CoTraining(), {
                'pos': [1],
                'neg': [1],
                'ind1': [np.linspace(0, 6, 7, dtype=int)],
                'ind2': [np.linspace(7, 15, 9, dtype=int)]
            })
        ]

        # datasets
        datasets = [
            ('house', None, None, None, None),
            ('house', None, None,
                    os.path.join(os.path.dirname(__file__), 'split'), None)
        ]

        for name, feature_file, label_file, split_path, _ in datasets:
            if split_path:
                if not os.path.exists(os.path.join(split_path, 'split', name)):
                    X, y = load_dataset(name, feature_file, label_file)
                    _, test_idxs, labeled_idxs, unlabeled_idxs = \
                        data_manipulate.inductive_split(X=X, y=y,
                                                        test_ratio=test_ratio,
                                                        initial_label_rate=(
                                                        1 - test_ratio)
                                                        * (1 - unlabel_ratio),
                                                        save_file=True,
                                                        split_count=number_init,
                                                        all_class=all_class,
                                                        saving_path=split_path,
                                                        name=name)

        experiments = SslExperimentsWithoutGraph(transductive=False, n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)

        experiments.set_metric(performance_metric='accuracy_score')
        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=10)

        for acc in results.get('house').get('CoTraining'):
            self.assertEqual(acc > 0.5, True)
        
        ###########
        experiments = SslExperimentsWithoutGraph(transductive=False, n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)

        experiments.set_metric(performance_metric='zero_one_loss',
                                metric_large_better=False)
        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=10)
        
        for acc in results.get('house').get('CoTraining'):
            self.assertEqual(acc < 0.5, True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestCoTraining))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())

    #
