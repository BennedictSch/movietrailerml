""" 
Unittest for `wrapper' package
Run in `test' folder
"""
import unittest

from safeu.datasets.base import load_clean1
from safeu.wrapper.sklearn_wrapper import SklearnRandomForestClassifier
from safeu.wrapper.sklearn_wrapper import SklearnLogisticRegression
from safeu.wrapper.sklearn_wrapper import SklearnGaussianNB
from safeu.wrapper.sklearn_wrapper import SklearnNearestNeighbors
from safeu.wrapper.sklearn_wrapper import SklearnMLPClassifier
from safeu.Experiments import SslExperimentsWithoutGraph


class TestSklearnWrapper(unittest.TestCase):
    def setUp(self):
        self.X, self.y = load_clean1(True)
    
    def tearDown(self):
        pass

    def test_params(self):
        model = SklearnRandomForestClassifier(n_estimators=10)
        self.assertEqual(model.model.n_estimators, 10)
        self.assertEqual(model.model.min_samples_split, 2)
        model.set_params({'n_estimators': 8, 'min_samples_split': 3})
        self.assertEqual(model.model.n_estimators, 8)
        self.assertEqual(model.model.min_samples_split, 3)

    def test_acc(self):
        '''
        '''
        configs = [
            ('RF', SklearnRandomForestClassifier(), {'n_estimators': [8]
            }),
            ('LR', SklearnLogisticRegression(), {}),
            ('NB', SklearnGaussianNB(), {}),
            ('NN', SklearnNearestNeighbors(), {}),
            ('MLP', SklearnMLPClassifier(), {})
        ]

        # datasets
        datasets = [
            ('house', None, None, None, None)
        ]
        experiments = SslExperimentsWithoutGraph(transductive=False, n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

        for acc in results.get('house').get('RF'):
            self.assertEqual(acc > 0.5, True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestSklearnWrapper))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())
