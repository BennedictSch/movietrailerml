""" 
Unittest for `classification' package
Run in `test' folder
"""
import unittest
import os
import warnings

import pandas as pd
import numpy as np
from math import isclose

import sklearn
import safeu
from safeu.datasets.data_manipulate import check_y
from safeu.Experiments import SslExperimentsWithGraph
import safeu.metrics.performance as perform
from safeu.classification.LPA import LPA
from sklearn.metrics import average_precision_score, f1_score


class TestPerformance(unittest.TestCase):
    def setUp(self):
        warnings.filterwarnings('ignore')
        pass

    def tearDown(self):
        pass

    def test_metrics_with_experiments(self):
        '''
        '''
        configs = [
        ('LPA', LPA(), {
            'kernel': ['rbf'],
        })
        ]

        datasets = [
            ('ionosphere', None, None, None, None)
        ]

        experiments = SslExperimentsWithGraph(n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')
        experiments.append_evaluate_metric(performance_metric='zero_one_loss')
        experiments.append_evaluate_metric(performance_metric='hamming_loss')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

        average_acc = np.mean(results.get('ionosphere').get('LPA'))
        self.assertEqual(average_acc > 0.5, True)

        _ = experiments.get_evaluation_results()

    def test_metrics_outside_experiments(self):
        cur_dir = os.path.abspath(os.path.curdir)
        pred_file = os.path.join(cur_dir, 'test', 'test_data', 'pred.csv')
        with open(pred_file, encoding='utf-8') as f:
            pred = pd.read_csv(f, delimiter=',', low_memory=False).get_values()

        validation_file = os.path.join(cur_dir, 'test', 'test_data',
                                       'validation.csv')
        with open(validation_file, encoding='utf-8') as f:
            validation = pd.read_csv(
                f, delimiter=',', low_memory=False).get_values()

        # binary
        methods_names = [
            'accuracy_score',
            'zero_one_loss',
            'roc_auc_score',
            'hamming_loss',
            'label_ranking_average_precision_score',
        ]

        for method_name in methods_names:
            func = getattr(safeu.metrics.performance, method_name)
            func_check = getattr(sklearn.metrics, method_name)
            self.assertEqual(isclose(
                func(validation, pred),
                func_check(validation, pred)), True)

        self.assertEqual(isclose(perform.Average_precision_score(
            validation, pred), average_precision_score(validation, pred)),
            True)
        self.assertEqual(isclose(perform.micro_auc_score(
            validation, pred), 0.8454106280193237), True)
        self.assertEqual(isclose(perform.F1_score(
            validation, pred), f1_score(validation, pred)), True)

        # multilabel
        _, validation_multilabel = check_y(validation, False)
        _, pred_multilabel = check_y(pred, False)

        methods_names = [
            'accuracy_score',
            'coverage_error',
            'label_ranking_loss',
            'hamming_loss',
            'label_ranking_average_precision_score',
        ]

        for method_name in methods_names:
            func = getattr(safeu.metrics.performance, method_name)
            func_check = getattr(sklearn.metrics, method_name)
            self.assertEqual(isclose(
                func(validation_multilabel, pred_multilabel),
                func_check(validation_multilabel, pred_multilabel)), True)

        self.assertEqual(isclose(perform.one_error(
            validation_multilabel, pred_multilabel), 1.), True)

        # regression
        pred_file_continuous = os.path.join(cur_dir, 'test', 'test_data',
                                            'pred_continuous.csv')
        with open(pred_file_continuous, encoding='utf-8') as f:
            pred_continuous = pd.read_csv(
                f, delimiter=',', low_memory=False, dtype=float).get_values()

        validation_file_continuous = os.path.join(cur_dir, 'test', 'test_data',
                                                  'validation_continuous.csv')
        with open(validation_file_continuous, encoding='utf-8') as f:
            validation_continuous = pd.read_csv(
                f, delimiter=',', low_memory=False, dtype=float).get_values()

        self.assertEqual(isclose(perform.minus_mean_square_error(
            validation_continuous, pred_continuous), -2223.4271225075877), True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestPerformance))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())

    #
