""" 
Unittest for `classification' package
Run in `test' folder
"""
import unittest

from safeu.datasets.base import load_clean1
from safeu.data_quality.LEAD import LEAD
from safeu.Experiments import SslExperimentsWithGraph


class TestLEAD(unittest.TestCase):
    def setUp(self):
        self.X, self.y = load_clean1(True)
    
    def tearDown(self):
        pass

    def test_params(self):
        model = LEAD()
        self.assertEqual(model.C1, 1.0)
        self.assertEqual(model.C2, 0.01)
        model.set_params({'C1': 2.0, 'C2': 0.05})
        self.assertEqual(model.C1, 2.0)
        self.assertEqual(model.C2, 0.05)

    def test_acc(self):
        '''
        '''
        configs = [
            ('LEAD', LEAD(), {
                'C1': [1.0],
                'C2': [0.01]
            })
        ]

        # datasets
        datasets = [
            ('spambase', None, None, None, None),
        ]
        experiments = SslExperimentsWithGraph(n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

        for acc in results.get('spambase').get('LEAD'):
            self.assertEqual(acc > 0.5, True)
        
        experiments = SslExperimentsWithGraph(n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='zero_one_loss',
                                metric_large_better=False)

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

        for acc in results.get('spambase').get('LEAD'):
            self.assertEqual(acc < 0.5, True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestLEAD))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())

    #
