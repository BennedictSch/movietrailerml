""" 
Unittest for `classification' package
Run in `test' folder
"""
import unittest
import numpy as np

from safeu.Experiments import SslExperimentsWithoutGraph
from safeu.datasets.data_manipulate import check_y, modify_y
from safeu.ensemble.SafetyForecast import SafetyForecast
from sklearn.neighbors import KNeighborsClassifier
from safeu.model_uncertainty.S4VM import S4VM
from safeu.datasets import data_manipulate, base
from safeu.metrics.performance import accuracy_score
from safeu.classification.TSVM import TSVM


class TestSLP(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_params(self):
        model = SafetyForecast()
        self.assertEqual(model.C1, 1.0)
        self.assertEqual(model.C2, 0.01)
        self.assertEqual(model.estimators, None)

        model.set_params({'C1': 0.05, 'C2': 10, 
                        'estimators': [(TSVM(), False)]})
        self.assertEqual(model.C1, 0.05)
        self.assertEqual(model.C2, 10)
        self.assertEqual(type(model.estimators[0][0]), TSVM)

    def test_acc_WPred(self):
        '''
        '''
        unlabel_ratio = 0.9

        estimator_list = [(TSVM(), False),
                        (S4VM(), True)]

        X, y = base.load_dataset('house', None, None)

        # split
        _, _, labeled_idxs, unlabeled_idxs =\
            data_manipulate.inductive_split(X=X, y=y, test_ratio=0.,
                                    initial_label_rate=1 - unlabel_ratio,
                                    split_count=1, all_class=True)

        labeled_idx = labeled_idxs[0]
        unlabeled_idx = unlabeled_idxs[0]

        predict_values = None
        # predict with given base estimators
        for estimator, transductive in estimator_list:
            estimator.fit(X, y, labeled_idx)

            temp_res = np.zeros_like(y)
            temp_res[labeled_idx] = y[labeled_idx]
            if transductive:
                temp_res[unlabeled_idx] = estimator.predict(
                    unlabeled_idx).reshape(-1, 1)
            else:
                temp_res[unlabeled_idx] = estimator.predict(
                    X[unlabeled_idx]).reshape(-1, 1)

            if predict_values is None:
                predict_values = temp_res
            else:
                predict_values = np.hstack((predict_values, temp_res))

        # provide baseline prediction
        n_labels, label = check_y(y, binary=True)
        knn = KNeighborsClassifier()
        knn.fit(X[labeled_idx], np.array(y).reshape(1, -1)[0][labeled_idx])
        baseline_pred = y
        baseline_pred[unlabeled_idx] = knn.predict(
            X[unlabeled_idx]).reshape(-1, 1)
        baseline_pred = modify_y(baseline_pred, range(
            0, len(baseline_pred)), n_labels)

        # fit SafetyForcast and predict in the following codes.
        ###############
        SF = SafetyForecast()
        SF.fit(predict_values, y, labeled_idx)
        pred = SF.predict(unlabeled_idx, baseline_pred=baseline_pred)

        accuracy_score(y[unlabeled_idx], pred)

    def test_acc_WEstimator(self):
        '''
        '''
        estimator_list = [(TSVM(), False),
                          (S4VM(), True)]

        configs = [
            ('SafetyForecast', SafetyForecast(), {
                'C1': [1.0],
                'C2': [0.01],
                'estimators': [estimator_list]
            })
        ]

        # datasets
        datasets = [
            ('house', None, None, None, None),
        ]
        experiments = SslExperimentsWithoutGraph(transductive=True, n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=2)

        average_acc = np.mean(results.get('house').get('SafetyForecast'))
        self.assertEqual(average_acc > 0.5, True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestSLP))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())
