""" 
Unittest for `classification' package
Run in `test' folder
"""
import unittest
import numpy as np

from safeu.datasets.base import load_clean1
from safeu.model_uncertainty.S4VM import S4VM
from safeu.Experiments import SslExperimentsWithoutGraph


class TestS4VM(unittest.TestCase):
    def setUp(self):
        self.X, self.y = load_clean1(True)
    
    def tearDown(self):
        pass

    def test_params(self):
        model = S4VM()
        self.assertEqual(model.kernel, 'RBF')
        self.assertEqual(model.C1, 100)
        self.assertEqual(model.C2, 0.1)
        self.assertEqual(model.sample_time, 100)
        self.assertEqual(model.gamma, 0)
        self.assertEqual(model.n_clusters, 10)
        model.set_params({'kernel': "Linear", 'C1': 50,
                         'C2': 0.2, 'sample_time': 200, 'gamma': 1,
                         'n_clusters': 5})
        self.assertEqual(model.kernel, 'Linear')
        self.assertEqual(model.C1, 50)
        self.assertEqual(model.sample_time, 200)
        self.assertEqual(model.gamma, 1)
        self.assertEqual(model.n_clusters, 5)

    def test_acc(self):
        '''
        '''
        configs = [
            ('S4VM', S4VM(), {
                'kernel': 'RBF',
                'gamma': [0],
                'C1': [100],
                'C2': [0.1],
                'sample_time': [100],
                'n_clusters': [5]
            }),
        ]

        # datasets
        datasets = [
            ('house', None, None, None, None),
            # ('isolet', None, None, None, None),
            # ('clean1', None, None, None, None)
        ]
        experiments = SslExperimentsWithoutGraph(transductive=True, n_jobs=4)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='accuracy_score')

        results = experiments.experiments_on_datasets(
            unlabel_ratio=0.8, test_ratio=0.2, number_init=1)

        # print(results)

        # for acc in results.get('australian').get('S4VM'):
        #    self.assertEqual(acc > 0.7, True)
        average_acc = np.mean(results.get('house').get('S4VM'))
        print(average_acc)
        self.assertEqual(average_acc > 0.5, True)
        # for acc in results.get('clean1').get('S4VM'):
        #    self.assertEqual(acc > 0.54, True)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestS4VM))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())

    #
