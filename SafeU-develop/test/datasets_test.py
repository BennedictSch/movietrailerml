""" 
Unittest for `datasets' package
Run in `test' folder
""" 
import unittest
import os
import numpy as np
from safeu.datasets import data_manipulate
import safeu.datasets.base
import safeu.datasets.usps
from safeu.datasets.base import load_data
from safeu.datasets.base import load_dataset
from safeu.datasets.base import load_graph_diff_formats
from safeu.datasets.base import load_graph
from sklearn.utils import Bunch
from sklearn.datasets import make_classification


class TestDataManipulate(unittest.TestCase):
    def setUp(self):
        self.X, self.Y = make_classification()
        _, self.Ymat = data_manipulate.check_y(self.Y, binary=False)
    
    def tearDown(self):
        pass
    
    def test_inductive_split(self):
        '''
        Unittest data_manipulate.inductive_split()
        '''
        # without index
        train_idx, test_idx, label_idx, unlabel_idx = \
            data_manipulate.inductive_split(self.X, self.Y)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # with index
        train_idx, test_idx, label_idx, unlabel_idx = \
            data_manipulate.inductive_split(self.X, self.Y, 
                np.arange(self.X.shape[0]))
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # all_class = False
        train_idx, test_idx, label_idx, unlabel_idx = \
            data_manipulate.inductive_split(self.X, self.Y, all_class=False)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # y = None, all_class=False
        train_idx, test_idx, label_idx, unlabel_idx = \
            data_manipulate.inductive_split(self.X, None,
                np.arange(self.X.shape[0]), all_class=False)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())

        # Exception tests
        with self.assertRaisesRegex(ValueError,
                "Must provide one of X, y or instance_indexes."):
            _, _, _, _ = data_manipulate.inductive_split(None, None)

        with self.assertRaisesRegex(ValueError,
                "Different length of instances and _labels found."):
            _, _, _, _ = data_manipulate.inductive_split(self.X, self.Y[:-1])
        
        with self.assertRaisesRegex(TypeError,
                "A list or np.ndarray object is expected, but received:.+"):
            _, _, _, _ = data_manipulate.inductive_split(None, None,
                                instance_indexes=(1, 2, 3), all_class=False)

        with self.assertRaisesRegex(ValueError,
                "y must be provided when all_class flag is True."):
            _, _, _, _ = data_manipulate.inductive_split(self.X, None,
                np.arange(self.X.shape[0]))
        
        with self.assertRaisesRegex(ValueError,
                "The initial rate is too small to guarantee that each " 
                "split will contain at least one instance for each class."):
            _, _, _, _ = data_manipulate.inductive_split(self.X, self.Y,
                initial_label_rate=0.00001)
        
        with self.assertRaisesRegex(ValueError,
                "Instance index out of range"):
            _, _, _, _ = \
            data_manipulate.inductive_split(self.X, self.Y, 
                np.arange(self.X.shape[0] + 1)[1:])
    
    def test_ratio_split(self):
        '''
        Unittest data_manipulate.ratio_split()
        '''
        # without index
        train_idx, test_idx = \
            data_manipulate.ratio_split(self.X, self.Y)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # with index
        train_idx, test_idx = \
            data_manipulate.ratio_split(self.X, self.Y, 
                np.arange(self.X.shape[0]))
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # all_class = False
        train_idx, test_idx = \
            data_manipulate.ratio_split(self.X, self.Y, all_class=False)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # y = None, all_class=False
        train_idx, test_idx = \
            data_manipulate.ratio_split(self.X, None,
                np.arange(self.X.shape[0]), all_class=False)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())

        # Exception tests
        with self.assertRaisesRegex(ValueError,
                "Must provide one of X, y or instance_indexes."):
            _, _ = data_manipulate.ratio_split(None, None)

        with self.assertRaisesRegex(ValueError,
                "Different length of instances and _labels found."):
            _, _ = data_manipulate.ratio_split(self.X, self.Y[:-1])
        
        with self.assertRaisesRegex(TypeError,
                "A list or np.ndarray object is expected, but received:.+"):
            _, _ = data_manipulate.ratio_split(None, None,
                                instance_indexes=(1, 2, 3), all_class=False)

        with self.assertRaisesRegex(ValueError,
                "y must be provided when all_class flag is True."):
            _, _ = data_manipulate.ratio_split(self.X, None,
                np.arange(self.X.shape[0]))
        
        with self.assertRaisesRegex(ValueError,
                "Instance index out of range"):
            _, _ = data_manipulate.ratio_split(self.X, self.Y, 
                np.arange(self.X.shape[0] + 1)[1:])
    
    def test_cv_split(self):
        '''
        Unittest data_manipulate.cv_split()
        '''
        # without index
        train_idx, test_idx = \
            data_manipulate.cv_split(self.X, self.Y)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # proba matrix
        train_idx, test_idx = \
            data_manipulate.cv_split(self.X, self.Ymat)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # proba matrix, multi-label
        YMul = self.Ymat
        YMul[:, 0] = 1
        train_idx, test_idx = \
            data_manipulate.cv_split(self.X, YMul, all_class=False)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # with index
        train_idx, test_idx = \
            data_manipulate.cv_split(self.X, self.Y, 
                np.arange(self.X.shape[0]))
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # all_class = False
        train_idx, test_idx = \
            data_manipulate.cv_split(self.X, self.Y, all_class=False)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())
        
        # y = None, all_class=False
        train_idx, test_idx = \
            data_manipulate.cv_split(self.X, None,
                np.arange(self.X.shape[0]), all_class=False)
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.X.shape[0])
        self.assertEqual(train_idx[0].shape[0] + test_idx[0].shape[0], 
                         self.Y.shape[0])
        for train, test in zip(train_idx, test_idx):
            self.assertEqual(set(train) & set(test), set())

        # Exception tests
        with self.assertRaisesRegex(ValueError,
                "Must provide one of X, y or instance_indexes."):
            _, _ = data_manipulate.cv_split(None, None)

        with self.assertRaisesRegex(ValueError,
                "Different length of instances X and _labels Y found."):
            _, _ = data_manipulate.cv_split(self.X, self.Y[:-1])
        
        with self.assertRaisesRegex(TypeError,
                "A list or np.ndarray object is expected, but received:.+"):
            _, _ = data_manipulate.cv_split(self.X, self.Y,
                                instance_indexes=(1, 2, 3), all_class=False)

        with self.assertRaisesRegex(ValueError,
                "y must be provided when all_class flag is True."):
            _, _ = data_manipulate.cv_split(self.X, None,
                np.arange(self.X.shape[0]))
        
        with self.assertRaisesRegex(ValueError,
                "Each instance belongs to one class!"):
            train_idx, test_idx = data_manipulate.cv_split(self.X, YMul)
        
    def test_load_data(self):
        methods_names = [
           'load_boston',
           'load_diabetes',
           'load_digits',
           'load_iris',
           'load_breast_cancer',
           'load_linnerud',
           'load_wine',
           'load_ionosphere',
           'load_australian',
           'load_bupa',
           'load_haberman',
           'load_vehicle',
           'load_covtype',
           'load_spambase',
           'load_house',
           'load_clean1',
           'load_isolet',
           'load_usps',
           ]
        for method_name in methods_names:
            func = getattr(safeu.datasets.base, method_name)
            data, target = func(return_X_y=True)
            self.assertIsNotNone(data)
            self.assertIsNotNone(target)
            bunch = func()
            self.assertIsInstance(bunch, Bunch)

        bunch_usps = safeu.datasets.usps.load_usps()
        self.assertIsInstance(bunch_usps, Bunch)

        bunch_covtype = safeu.datasets.covtype.load_covtype()
        self.assertIsInstance(bunch_covtype, Bunch)

        bunch_Gcovtype = safeu.datasets.covtype.load_graph_covtype()
        self.assertIsInstance(bunch_Gcovtype, Bunch)

        try:
            X, y = load_dataset('A', None, None)
            X, y = load_dataset(None, None, None)
        except ValueError:
            pass

        cur_dir = os.path.abspath(os.path.curdir)
        feature_file = os.path.join(cur_dir, 'test', 'test_data',
                                    'sample_feature.csv')
        label_file = os.path.join(cur_dir, 'test', 'test_data',
                                                'sample_label.csv')
        X, y = load_data(feature_file=feature_file, label_file=label_file)
        self.assertIsNotNone(X)
        self.assertIsNotNone(y)
        
        feature_file = os.path.join(cur_dir, 'test', 'test_data',
                                                'sample_7_2.mat')
        label_file = os.path.join(cur_dir, 'test', 'test_data',
                                                'sample_7_2.mat')
        X, y = load_data(feature_file=feature_file, label_file=label_file)
        self.assertIsNotNone(X)
        self.assertIsNotNone(y)

    def test_load_graph(self):
        # load_graph_diff_formats
        cur_path = os.path.abspath(os.path.curdir)
        W = load_graph_diff_formats(os.path.join(cur_path, 'test', 'test_data',
                                                'sample_7_2.mat'))
        self.assertIsNotNone(W)
        W = load_graph_diff_formats(os.path.join(cur_path, 'test', 'test_data',
                                                'sample_7_3.mat'))
        self.assertIsNotNone(W)

        # load_graph
        W = load_graph('covtype')
        self.assertIsNotNone(W)
        W = load_graph('cov', os.path.join(cur_path, 'test', 'test_data',
                                                'sample_7_2.mat'))
        self.assertIsNotNone(W)
        W = load_graph('cov')
        self.assertIsNone(W)
        W = load_graph(None, None)
        self.assertIsNone(W)
        W = load_graph(None, os.path.join(cur_path, 'test', 'test_data',
                                                'sample_7_2.mat'))
        self.assertIsNotNone(W)                                     


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestDataManipulate))
    # suite.addTest(WidgetTestCase('test_default_widget_size'))
    # suite.addTest(WidgetTestCase('test_widget_resize'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())
