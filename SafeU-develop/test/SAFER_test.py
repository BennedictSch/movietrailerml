""" 
Unittest for `classification' package
Run in `test' folder
"""
import unittest

from safeu.datasets.base import load_clean1
from safeu.model_uncertainty.SAFER import SAFER
from safeu.Experiments import SslExperimentsWithoutGraph


class TestSafer(unittest.TestCase):
    def setUp(self):
        self.X, self.y = load_clean1(True)
    
    def tearDown(self):
        pass

    def test_params(self):
        model = SAFER()
        self.assertIsNone(model.baseline_prediction)
        self.assertIsNone(model.prediction)
        self.assertEqual(model.Safer_prediction, 0.0)
        self.assertEqual(model.estimator, False)
        model.set_params({'estimator': True})
        self.assertEqual(model.estimator, True)

    def test_acc(self):
        configs = [
        ('SAFER', SAFER(), {
            'estimator': 'True'})
        ]

        # datasets
        datasets = [
            ('boston', None, None, None, None),
        ]
        experiments = SslExperimentsWithoutGraph(transductive=True, n_jobs=1)

        experiments.append_configs(configs)
        experiments.append_datasets(datasets)
        experiments.set_metric(performance_metric='minus_mean_square_error')

        _ = experiments.experiments_on_datasets(
            unlabel_ratio=0.75, test_ratio=0.2, number_init=4)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(TestSafer))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())

    #
