""" 
Unittest for `classification' package
Run in `test' folder
""" 
from safeu.model_uncertainty.SAFER import SAFER
from safeu.model_uncertainty.S4VM import S4VM
from safeu.data_quality.SLP import SLP
from safeu.data_quality.LEAD import LEAD
from safeu.classification.TSVM import TSVM
from safeu.classification.LPA import LPA
from safeu.classification.CoTraining import CoTraining
import os
import sys
import unittest

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


class TestBaseEstimatorRepr(unittest.TestCase):
    """ Try logging the message of all estimators.
    Test methods:
        `BaseEstimator._get_attr_names`
        `BaseEstimator.get_params`
        `BaseEstimator.__repr__`
    """

    def setUp(self):
        self.cot = CoTraining()
        self.lpa = LPA()
        self.tsvm = TSVM()
        self.lead = LEAD()
        self.slp = SLP()
        self.s4vm = S4VM()
        self.safer = SAFER()
    
    def tearDown(self):
        pass

    def test_params(self):
        print("Examples logging the messages of estimators:")
        print(self.cot)
        print(self.lpa)
        print(self.tsvm)
        print(self.lead)
        print(self.slp)
        print(self.s4vm)
        print(self.safer)


def generate_suite():
    """ Return a unittest.TestSuite object
    Organize the unittest suite.
    """
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
        TestBaseEstimatorRepr))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(generate_suite())
