"""
This module wraps existing supervised learning algorithms in sklearn
"""

__all__ = ['sklearn_wrapper']