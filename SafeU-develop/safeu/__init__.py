"""
Safe Semi-Supervised Learning (SafeU) for Python
==================================

SafeU is a Python module integrating safe semi-supervised learning 
algorithms using scientific Python packages (numpy, scipy, matplotlib) 
and with the help of sklearn module (See http://scikit-learn.org).

"""
'''import sys
import re
import warnings
import os
from contextlib import contextmanager as _contextmanager
import logging'''

__all__ = [
    'datasets',
    'utils',
    'data_quality',
    'datasets',
    'Experiments',
    'base'
    ]
