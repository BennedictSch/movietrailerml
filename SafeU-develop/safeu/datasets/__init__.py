"""
The :mod:`sklearn.datasets` module includes utilities to load datasets,
including methods to load and fetch popular reference datasets. It also
features some artificial data generators.
"""
# from . import base
# from . import data_manipulate
# from . import usps

__all__ = ['base',
           'data_manipulate',
           'usps',
           'covtype']
