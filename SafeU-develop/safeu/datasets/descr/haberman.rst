Haberman's Survival Database
============================

Notes
-----
Data Set Characteristics:
	:Number of Instances: 306
	:Number of Attributes: 3 
	:Attribute Information:
		- Age of patient at time of operation (numerical)
		- Patient's year of operation (year - 1900, numerical)
		- Number of positive axillary nodes detected (numerical)
		- Survival status (class attribute)
			1 = the patient survived 5 years or longer
			2 = the patient died within 5 year	
	:Summary Statistics:
	
	======================================================= ==== ==== ======= ======
															Min  Max   Mean    SD   
    ======================================================= ==== ==== ======= ======
    Age of patient at time of operation:   					30   83   52.46   10.79   
    Patient's year of operation (year - 1900, numerical):   58   69   62.85   3.24  
    Number of positive axillary nodes detected (numerical): 0    52   4.03    7.18
    ======================================================= ==== ==== ======= ======		

	:Missing Attribute Values: None
    :Donor: Tjen-Sien Lim (limt@stat.wisc.edu)
    :Date: March 4, 1999
	
	
   
Relevant Information:
   The dataset contains cases from a study that was conducted between
   1958 and 1970 at the University of Chicago's Billings Hospital on
   the survival of patients who had undergone surgery for breast
   cancer.
   
References
----------
	- Haberman, S. J. (1976). Generalized Residuals for Log-Linear
      Models, Proceedings of the 9th International Biometrics
      Conference, Boston, pp. 104-122.
    - Landwehr, J. M., Pregibon, D., and Shoemaker, A. C. (1984),
      Graphical Models for Assessing Logistic Regression Models (with
      discussion), Journal of the American Statistical Association 79:
      61-83.
    - Lo, W.-D. (1993). Logistic Regression Trees, PhD thesis,
      Department of Statistics, University of Wisconsin, Madison, WI.q