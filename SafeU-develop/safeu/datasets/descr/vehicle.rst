Vehicle Database
================

Notes
-----

Data Set Characteristics:
    :Number of Instances: 596
    :Number of Attributes: 18 numeric, predictive attributes and the class
        All attributes are continuous, real-valued attributes scaled into
        the range -1.0 to 1.0.
						  
    :Missing Attribute Values: none(missing data has been dropped)
		
		
Relevant Information:
    We dropped the missing data, and you can find origin data set from
    https://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/multiclass/vehicle.scale