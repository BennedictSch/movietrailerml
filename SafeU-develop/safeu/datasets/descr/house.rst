House Data Database
===================

Notes
-----
Data Set Characteristics:
    :Number of Instances: 232
    :Number of Attributes: 16 numeric, predictive attributes and the class
    :Missing Attribute Values: None
    :Class Distribution: class_0 (108), class_1 (124)