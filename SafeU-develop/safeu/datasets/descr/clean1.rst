Clean1 Data Database
====================

Notes
-----
Data Set Characteristics:
    :Number of Instances: 476
    :Number of Attributes: 166 numeric, predictive attributes and the class
    :Missing Attribute Values: None
    :Class Distribution: class_0 (207), class_1 (269)