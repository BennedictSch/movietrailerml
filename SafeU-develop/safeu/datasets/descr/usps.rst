USPS Database
=============

Notes
-----
Data Set Characteristics:
	:Number of Instances: usps:  7291; usps_test: 2007
    :Number of Attributes: 256 All attributes are continuous, real-valued attributes scaled into the
        range -1.0 to 1.0.
						  
    :Missing Attribute Values: none
						 
Relevant Information Paragraph:
    Handwritten recognition for digital acquisition. There are a total of 9298
    handwritten digital images in the library(both 16*16 pixel grayscale values,
	the grayscale values have been normalized).
