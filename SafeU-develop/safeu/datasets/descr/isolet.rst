ISOLET Database
===============

Notes
-----
Data Set Characteristics:
	:Number of Instances: 600
	:Number of Attributes: 50 numeric, predictive attributes and the class
    	All attributes are continuous, real-valued attributes scaled into
        the range -1.0 to 1.0.
	:Missing Attribute Values: none
