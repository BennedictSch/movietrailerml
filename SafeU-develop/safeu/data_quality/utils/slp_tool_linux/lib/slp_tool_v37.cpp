#define BOOST_PYTHON_STATIC_LIB
#define BOOST_NUMPY_STATIC_LIB
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/numpy.hpp>
#include <boost/progress.hpp>
#include <iostream>
#include <algorithm>
#include <stdlib.h>

namespace p = boost::python;
namespace np = p::numpy;

std::vector<int*> find2cells(p::object row, p::object col, size_t* l_ind, int n)
{
	np::initialize();
	np::ndarray row_temp = np::array(row);
	np::ndarray col_temp = np::array(col);
	size_t edges = len(row_temp);
	int* row_ = reinterpret_cast<int*> (row_temp.get_data());
	int* col_ = reinterpret_cast<int*> (col_temp.get_data());

	std::vector<int*> re;
	int i = 0, k = 0;

	int count = 0;
	int cur = i;
	int pre = i;

	for (; i < edges; ++i)
	{
		if (col_[i] != cur)
		{
			int* re_temp = new int[count+1];
			re_temp[0] = count;
			for (int j = i - count, z = 1; j < i; ++j, ++z)
				re_temp[z] = row_[j];

			re.push_back(re_temp);

			count = 1;
			cur = col_[i];

			int gap = cur - pre;
			while (gap-- > 1)
				re.push_back(NULL);
			pre = cur;
		}
		else
			++count;

	}

	int* re_temp = new int[count+1];
	re_temp[0] = count;
	for (int j = edges - count, z = 1; j < edges; ++j, ++z)
		re_temp[z] = row_[j];
	re.push_back(re_temp);
	int gap = n - pre;
	while (gap-- > 1)
		re.push_back(NULL);


	return re;
}

p::object iteration(p::object row, p::object col, p::object l_ind, int l, int n,
	p::object f, p::object Jc, p::object W_data, double stepSize, int steps)
{
	np::initialize();

	np::ndarray l_ind_temp = np::array(l_ind);
	size_t* l_ind_ = reinterpret_cast<size_t*> (l_ind_temp.get_data());
	std::sort(l_ind_, l_ind_ + l);

	std::vector<int*> lnonzeros = find2cells(row, col, l_ind_, n);

	np::ndarray data_f = np::array(f);
	int frow = data_f.shape(0);
	int fcol = data_f.shape(1);
	double* f_ = reinterpret_cast<double*> (data_f.get_data());

	np::ndarray Jc_temp = np::array(Jc);
	int* Jc_idx = reinterpret_cast<int*> (Jc_temp.get_data());
	np::ndarray W_temp = np::array(W_data);
	double* W_data_ = reinterpret_cast<double*> (W_temp.get_data());
	int len_data = len(W_data);

	double stepSize1 = stepSize;
	float var = 1.0;


	for (int it = 0; it < l; ++it)
	{
		int id = l_ind_[it];
		int* cell_ele = lnonzeros[id];
		if (cell_ele != NULL)
		{
			
			int cell_ele_size = cell_ele[0];
			//std::cout << cell_ele_size << std::endl;
			for (int j = 0; j < cell_ele_size - 1; ++j)
			{
				int data_ind = Jc_idx[id] + j;
				if (data_ind < len_data)
				{
					int ind = cell_ele[j+1] * fcol;
					int ind2 = it * fcol;
					for (int d = 0; d < fcol; ++d)
						f_[ind + d] -= stepSize * W_data_[data_ind] * (f_[ind + d] - f_[ind2 + d]);
				}
			}
		}
	}


	for (int a = 0; a < steps; ++a)
	{
		//boost::progress_timer pt1;
		int it = rand() % frow;
		for (int k = 0; k < frow; ++k)
		{
			it = (it + 1) % frow;
			int* cell_ele = lnonzeros[it];
			if (cell_ele != NULL)
			{
				int cell_ele_size = cell_ele[0];
				for (int j = 0; j < cell_ele_size - 1; ++j)
				{
					int data_ind = Jc_idx[it] + j;
					if (data_ind < len_data)
					{
						int ind = cell_ele[j+1] * fcol;
						int ind2 = it * fcol;
						for (int i = 0; i < fcol; ++i)
							f_[ind + i] -= stepSize * W_data_[data_ind] * (f_[ind + i] - f_[ind2 + i]);
						stepSize = stepSize1 / sqrt(var++);
					}
				}
			}
		}
	}

	f = np::from_data(f_, np::dtype::get_builtin<double>(), p::make_tuple(frow, fcol), p::make_tuple(8 * fcol, 8), f);
	return f;
}

BOOST_PYTHON_MODULE(slp_tool_v37)
{
	using namespace boost::python;
	def("iteration", iteration);
}
