def get_connected_component(W):
    """
    :param W: graph matrix
    :return component: ind: the data indexes of each connected component
                        n: the number of connected components
    """
