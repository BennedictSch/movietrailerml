APIs
====

The following links contain detailed information about every function, class and module.

.. toctree::
   :maxdepth: 3
   
   safeu/subpackages
   safeu/submodules