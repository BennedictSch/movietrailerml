
Tutorial of SafeU
=================

This documentation introduces the SafeU. Examples are provided to show the generic usage under different settings. To master SafeU, a careful reading of `Generic Processes th Use SafeU` and further reading of `User Guide` will be much helpful.

.. toctree::
   :maxdepth: 2
   
   Quick Start
   Generic Processes to Use SafeU

