performance
===========

.. automodule:: safeu.metrics.performance
    :members:
    :undoc-members:
    :show-inheritance:
