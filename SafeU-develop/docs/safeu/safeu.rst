safeu package
===========

Subpackages
-----------

.. toctree::

    classification
    data_quality
    datasets
    ensemble
    metrics
    model_uncertainty
    utils

Submodules
----------

.. toctree::

   Experiments
   base

