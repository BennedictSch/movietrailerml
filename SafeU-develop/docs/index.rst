.. safeu documentation master file, created by
   sphinx-quickstart on Thu May  2 11:21:44 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to safeu's documentation!
=================================

SafeU is a python toolkit based on safe semi-supervised learning, which is a novel branch of semi-supervised learning. 

Semi-supervised learning (SSL) concerns the problem on how to improve learning performance via the usage of a small amount of labeled data and a large amount of unlabeled data. Many SSL methods have been developed, e.g., generative model, graph-based method, disagreement-based method and semi-supervised SVMs. Despite the success of SSL, however, a considerable amount of empirical studies reveal that SSL with the exploitation of unlabeled data might even deteriorate learning performance. It is highly desirable to study **safe** SSL scheme that on one side could often improve performance, on the other side will not hurt performance, since the users of SSL wont expect that SSL with the usage of more data performs worse than certain direct supervised learning with only labeled data. 

Specifically, *Safe*, here means that the generalization performance is never *statistically significantly worse* than methods using only labeled data.

SafeU implements multiple front-end safe semi-supervised learning algorithms, and provides a weakly-supervised learning experiment framework including some well-defined protocols for learning algorithms, experiments and evaluation metrics. With this toolkit, you can build up your comparing experiments between learning algorithms with different learning settings like supervised, semi/weakly-supervised, as well as different tasks such as single/multi-label learning. We hope this toolkit could help you explore the classic semi-supervised learning algorithms and go further to test your ones.

Submit bugs or suggestions in the Issues section or feel free to submit your contributions as a pull request.

Table of Content
----------------

.. toctree::
   :maxdepth: 3
   
   Introduction
   Tutorial of SafeU
   APIs
   User Guide
   Reference
   News


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

