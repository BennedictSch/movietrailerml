
Quick Start
=============
SafeU is a python package of `Safe Semi-Supervised Learning <https://git.nju.edu.cn/lamda/safeu>`_.


Dependencies 
------------
The package is developed with Python3 (version 3.6|3.7 are tested in both ``windows`` and ``linux`` system).

Basic Dependencies

::

    numpy >= 1.15.1
    scipy >= 1.1.0
    scikit-learn >= 0.19.2
    cvxopt >= 1.2.0

Setup
-----
You can get safeu simply by:

::

    $ pip install safeu


Or clone safeu source code to your local directory and build from source:

::

    $ cd SafeU
    $ python setup.py safeu
    $ pip install dist/*.whl

Both ways would install the dependent packages with `pip` command automatically.

A Quick Example
---------------

We can use safeu for different experiments. The following example shows a possible way to do experiments based on built-in algorithms and data sets:

.. code:: python

    import sys, os
    from safeu.Experiments import SslExperimentsWithoutGraph
    from safeu.model_uncertainty.S4VM import S4VM

    # algorithm configs
    configs = [
            ('S4VM', S4VM(), {
                'kernel': 'RBF',
                'gamma':[0],
                'C1': [50,100],
                'C2': [0.05,0.1],
                'sample_time':[100]
            })
        ]

    # datasets
    # name,feature_file,label_file,split_path,graph_file
    datasets = [
        ('house', None, None, None, None),
        ('isolet', None, None, None, None)
        ]

    # experiments
    experiments = SslExperimentsWithoutGraph(transductive=True, n_jobs=4)
    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(unlabel_ratio=0.75,test_ratio=0.2,
        number_init=2)
