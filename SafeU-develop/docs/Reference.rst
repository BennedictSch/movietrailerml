
Reference
=========

This package is built based on the following works:

`Works led by Dr. Li <http://lamda.nju.edu.cn/liyf/>`_.

#. `Yu-Feng Li, De-Ming Liang. Safe Semi-Supervised Learning: A Brief Introduction. Frontiers of Computer Science (FCS). in press. <http://lamda.nju.edu.cn/liyf/paper/FCS19-SafeSSL.pdf>`_
#. `De-Ming Liang, Yu-Feng Li. Lightweight label propagation for large-scale network data. In: Proceedings of the 27th International Joint Conference on Artificial Intelligence (IJCAI'18), Stockholm, Sweden, 2018, pp.3421-3427. <https://www.ijcai.org/proceedings/2018/0475.pdf>`_
#. `Yu-Feng Li, Han-Wen Zha, Zhi-Hua Zhou. Learning safe prediction for semi-supervised regression. In: Proceedings of the 31st AAAI conference on Artificial Intelligence (AAAI'17), San Francisco, CA, 2017, pp.2217-2223. <https://aaai.org/ocs/index.php/AAAI/AAAI17/paper/view/14587/14396>`_
#. `Yu-Feng Li, Shao-Bo Wang, Zhi-Hua Zhou. Graph quality judgement: A large margin expedition. In: Proceedings of the 25th International Joint Conference on Artificial Intelligence (IJCAI'16), New York, NY, 2016, pp.1725-1731. <https://www.ijcai.org/Proceedings/16/Papers/247.pdf>`_
#. `Yu-Feng Li, James Kwok and Zhi-Hua Zhou. Towards safe semi-supervised learning for multivariate performance measures. In: Proceedings of the 30th AAAI conference on Artificial Intelligence (AAAI'16), Phoenix, AZ, 2016, pp. 1816-1822. <http://lamda.nju.edu.cn/liyf/paper/aaai16-umvp.pdf>`_
#. `Yu-Feng Li and Zhi-Hua Zhou. Towards making unlabeled data never hurt. IEEE Transactions on Pattern Analysis and Machine Intelligence (TPAMI), 37(1):175-188, 2015. <http://lamda.nju.edu.cn/liyf/paper/TPAMI15-S4VM.pdf>`_
#. `Yu-Feng Li, Ivor Tsang, James Kwok and Zhi-Hua Zhou. Convex and Scalable Weakly Labeled SVMs. Journal of Machine Learning Research (JMLR), 14:2151-2188, 2013. <http://lamda.nju.edu.cn/liyf/paper/jmlr13-wellsvm.pdf>`_
#. `Yu-Feng Li and Zhi-Hua Zhou. Towards making unlabeled data never hurt. In: Proceedings of the 28th International Conference on Machine Learning (ICML'11), Bellevue, WA, 2011, pp.1081-1088. <http://lamda.nju.edu.cn/liyf/paper/icml11-s4vm.pdf>`_

Semi-supervised Learning

#. `Blum, Avrim, and Tom Mitchell. Combining labeled and unlabeled data with co-training. In: Proceedings of the 11th Annual Conference on Computational Learning Theory (COLT'98), New York, NY, 1998, pp.92-100. <https://www.cs.cmu.edu/~avrim/Papers/cotrain.pdf>`_
#. `Joachims, Thorsten. Transductive Inference for Text Classication using Support Vector Machines. In: Proceedings of the 16th International Conference on Machine Learning (ICML'99), San Francisco, CA, 1999. pp.200-209. <https://www.cs.cornell.edu/people/tj/publications/joachims_99c.pdf>`_
#. `Zhu, Xiaojin, Zoubin Ghahramani, and John D. Lafferty. Semi-Supervised Learning Using Gaussian Fields and Harmonic Functions. In: Proceedings of the 20th International conference on Machine learning (ICML'03), Washington, DC, 2003, pp. 912-919. <http://mlg.eng.cam.ac.uk/zoubin/papers/zgl.pdf>`_

Third-party Library


#. `Pedregosa, Fabian, Gaël Varoquaux, Alexandre Gramfort, Vincent Michel, Bertrand Thirion, Olivier Grisel, Mathieu Blondel et al. Scikit-learn: Machine learning in Python. Journal of machine learning research (JMLR), 12: 2825-2830, 2013. <https://scikit-learn.org/stable/>`_
#. `C.-C. Chang and C.-J. Lin. LIBSVM : a library for support vector machines. ACM Transactions on Intelligent Systems and Technology, 2:27:1--27:27, 2011.  <https://www.csie.ntu.edu.tw/~cjlin/libsvm/>`_.
#. `R.-E. Fan, K.-W. Chang, C.-J. Hsieh, X.-R. Wang, and C.-J. Lin. LIBLINEAR: A library for large linear classification . Journal of Machine Learning Research (JMLR), 9: 1871-1874, 2008. <https://www.csie.ntu.edu.tw/~cjlin/liblinear/>`_.
