----
News
----

Plans and Release Note
======================

v0.1.0
------
First release!

1. A general semi-supervised learning experiment framework.
2. Classical semi-supervised learning algorithms.
3. Some of our explorations on safe semi-supervised learning.



