
User Guide
==========

This section details the customization process and implementation of the built-in classes and povides further instructions for advanced users.


.. toctree::
   :maxdepth: 2
   
   Usage of Built-in Experiments
   Details of Built-in Estimators
   List of Built-in Metrics
   How to Implement Your Own Estimators
   Prepare Data
