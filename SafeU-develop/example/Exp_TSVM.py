from safeu.Experiments import SslExperimentsWithoutGraph
from safeu.classification.TSVM import TSVM
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    configs = [
        ('TSVM', TSVM(), {
            'alpha': [0.1],
            'beta': [-1]})
    ]

    # datasets
    datasets = [
        ('clean1', None, None, None, None)

    ]
    experiments = SslExperimentsWithoutGraph(transductive=False, n_jobs=1)

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

    # do something on results #
