from safeu.Experiments import SslExperimentsWithGraph
from safeu.data_quality.SLP import SLP
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    configs = [

        ('SLP', SLP(), {
            'stepSize': [0.1],
            'T': [6]
        })
    ]

    # datasets
    datasets = [
        ('covtype', None, None, None, None)
    ]
    experiments = SslExperimentsWithGraph()

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=1 - 0.008, test_ratio=0.2, number_init=1)

    # do something on results #
