'''
This script shows how to config a SSL experiments with given
'''

from safeu.datasets import data_manipulate, base
from safeu.Experiments import SslExperimentsWithGraph
from safeu.data_quality.SLP import SLP
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


if __name__ == '__main__':
    test_ratio = 0.2
    unlabel_ratio = 0.75
    number_init = 5
    all_class = True

    configs = [
        ('SLP', SLP(), {
            'stepSize': [0.05, 0.1, 0.2],
            'T': [6, 10]
        })
    ]
    datasets = [
        ('covtype', None, None, os.path.join(
            os.path.dirname(__file__), 'split'), None)
    ]
    # [(name, feature_file, label_file, split_path, graph_file)]
    # When split_path is not None, a random split will be provided.

    ##############################################
    # The scirpt to split the dataset and save to split_path
    # provided existing split file, the experiment would use the given ones
    # instead of split the dataset again.
    if not os.path.exists(os.path.join(os.path.dirname(__file__), 'split')):
        os.mkdir(os.path.join(os.path.dirname(__file__), 'split'))
    for name, feature_file, label_file, split_path, _ in datasets:
        if split_path:
            if not os.path.exists(os.path.join(split_path, 'split', name)):
                X, y = base.load_dataset(name, feature_file, label_file)
                _, test_idxs, labeled_idxs, unlabeled_idxs =\
                    data_manipulate.inductive_split(X=X, y=y,
                                                    test_ratio=test_ratio,
                                                    initial_label_rate=(
                                                    1 - test_ratio)
                                                     * (1 - unlabel_ratio),
                                                    save_file=True,
                                                    split_count=number_init,
                                                    all_class=all_class,
                                                    saving_path=split_path,
                                                    name=name)

    experiments = SslExperimentsWithGraph()

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')
    results = experiments.experiments_on_datasets(unlabel_ratio=unlabel_ratio,
                                                  test_ratio=test_ratio,
                                                  number_init=number_init)

    # You can now do something with the results #
