from safeu.Experiments import SslExperimentsWithGraph
from safeu.data_quality.SLP import SLP
import sys
import os

from safeu.classification import LPA
from safeu.data_quality.LEAD import LEAD

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    configs = [
        ('SLP', SLP(), {
            'stepSize': [0.1],
            'T': [6]}),
        ('LPA', LPA(), {
            'kernel': ['rbf'],
            'gamma': [20],
            'n_neighbors': [7],
            'alpha': ['None'],
            'max_iter': [30],
            'tol': [1e-3]
        }),
        ('LEAD', LEAD(), {
            'C1': [1.0],
            'C2': [0.01]
        })
    ]

    # datasets
    datasets = [
        ('covtype', None, None, None, None),  # SLP
        ('spambase', None, None, None, None),  # LEAD
        ('isolet', None, None, None, None)  # LPA
    ]
    experiments = SslExperimentsWithGraph()

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

    # do something on results #
