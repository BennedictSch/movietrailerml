from safeu.data_quality.LEAD import LEAD
from sklearn.neighbors import KNeighborsClassifier
from sklearn.semi_supervised import label_propagation
import scipy.io as sio
import numpy as np
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


dataname = 'spambase'
C1 = 1
C2 = 0.01
T = 20


data = sio.loadmat('data/' + dataname + '.mat')
label = sio.loadmat('data/' + dataname + '_10.mat')
x = data['x']
y = data['y'] + 2
ind = label['l_ind']
l_ind = []
for l in ind:
    l_ind.extend(list(l[0][0]))

# print(l_ind)


labels = np.zeros(np.size(y)) - 1
# print(labels)
for index in l_ind:
    labels[index] = y[index]
# cnt = 0
# for i in y:
#    if i != 1:
#        cnt += 1
# print(cnt)
# print(labels[100:300])
# labels = y
# print(len(labels))
# print(len(y))
# print(np.array(y).reshape(1, -1)[0])
# value = []

model = LEAD()

for t in range(0, T):
    for n in range(2, 5):
        label_spread = label_propagation.LabelSpreading(kernel='knn', 
                n_neighbors=3 + n * 2, max_iter=200, tol=0.001)
        # kernel = 'knn', n_neighbors = 3 + n * 2, max_iter = 200, tol = 0.001
        label_spread.fit(x, np.array(labels))

        # label_spread.fit(x, np.array(y).reshape(1, -1)[0])
        # result = []
        if n == 2:
            result = label_spread.predict(x).reshape(-1, 1)
            # print(result)
        else:
            result = np.hstack((result,
                                label_spread.predict(x).reshape(-1, 1)))
        # value = np.hstack((value, result))
        # print(result)
        # cnt = 0
        # for i in result:
        #   if i != 1:
        #        cnt += 1
        # print(cnt)
    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(x[l_ind], np.array(y).reshape(1, -1)[0][l_ind])
    base = knn.predict(x).reshape(-1, 1)
    result = result - 2
    base = base - 2
    y = y - 2
    model.train(result, y, l_ind)
    pred, w = model.predict(base)
    u_ind = np.linspace(0, np.size(labels) - 1,
                        np.size(labels)).astype(np.int32)
    # print(len(u_ind))
    u_ind = np.delete(u_ind, l_ind)
    print(model._accuracy(pred[u_ind], y[u_ind]))
    # print(result)
