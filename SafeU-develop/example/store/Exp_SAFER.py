from safeu.Experiments import SslExperimentsWithoutGraph
from safeu.model_uncertainty.SAFER import SAFER
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    configs = [
        ('SAFER', SAFER(), {
            'estimator': 'True'})
    ]

    # datasets
    datasets = [
        ('housing10', None, None, None, None)
    ]
    experiments = SslExperimentsWithoutGraph(transductive=True, n_jobs=4)

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='minus_mean_square_error')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

    # do something on results #
