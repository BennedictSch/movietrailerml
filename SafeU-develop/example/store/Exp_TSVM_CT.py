from safeu.Experiments import SslExperimentsWithoutGraph
from safeu.classification.CoTraining import CoTraining
import numpy as np
import sys
import os

from safeu.classification import TSVM

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':

    configs = [
        ('TSVM', TSVM(), {
            'alpha': [0.1, 0.5],
            'beta': [-1]}),
        ('CoTraining', CoTraining(), {
            'pos': [207],
            'neg': [269],
            'ind1': np.linspace(0, 79, 80, dtype=int),
            'ind2': np.linspace(80, 164, 85, dtype=int)
        })
    ]

    # datasets
    datasets = [
        ('clean1', None, None, None, None)
    ]
    experiments = SslExperimentsWithoutGraph(transductive=False, n_jobs=4)

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=20)

    # do something on results #
