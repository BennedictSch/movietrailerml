from safeu.classification.TSVM import TSVM
import scipy.io as sio
import numpy as np
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


dataname = 'spambase'
C1 = 1
C2 = 0.01
T = 20

data2 = sio.loadmat('data/clean1,splits,labeled=10.mat')
data1 = sio.loadmat('data/clean1.mat')

X, y = data1['X'], data1['y']

results = []
for i in range(0, T):
    idx = np.arange(0, 476)
    np.random.shuffle(idx)
    # train data
    train_idx = idx[:400]
    train_instance = X[train_idx]
    train_label = y[train_idx]
    # labeled data index
    label_index = np.arange(0, 400)
    np.random.shuffle(label_index)
    label_index = label_index[:80]
    # test data
    test_idx = idx[400:]
    test_instance = X[test_idx]
    test_label = y[test_idx]
    # test
    model = TSVM()
    model.fit(train_instance, train_label, label_index)
    prediction = model.predict(test_instance)
    result = np.array((prediction == test_label).nonzero()
                      ).shape[1] / test_label.shape[0]
    results.append(result)
    # print(result)

print(results)
# idxUnls = data2['idxUnls']-1
# idxLabs = data2['idxLabs']-1
# index1 = idxLabs[0, :]
# index2 = idxUnls[0].reshape((1, -1))
# label_instance = X[index1, :]
# label = y[index1, :]
# unlabel_instance = X[index2][0]
# ground_truth = y
