from safeu.classification.TSVM import TSVM
from safeu.datasets import data_manipulate, base
import numpy as np
import sys
import os

from safeu.model_uncertainty.S4VM import S4VM

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    unlabel_ratio = 0.9

    # datasets
    datasets = [
        ('clean1', None, None)
    ]

    estimator_list = [(TSVM(), False),
                      (S4VM(), True)]

    # load dataset and split
    for name, feature_file, label_file in datasets:
        # load dataset
        X, y = base.load_dataset(name, feature_file, label_file)

        # split
        _, test_idxs, labeled_idxs, unlabeled_idxs = \
            data_manipulate.inductive_split(X=X, y=y, test_ratio=0.,
                                            initial_label_rate=1
                                                               - unlabel_ratio,
                                            split_count=1, all_class=True)

        labeled_idx = labeled_idxs[0]
        unlabeled_idx = unlabeled_idxs[0]

        predict_values = np.zeros([X.shape[0], len(estimator_list)])
        # predict with given base estimators
        for idx, estimator, fit_params in enumerate(estimator_list):
            estimator.fit(X, y, labeled_idx)
            predict_values[:, idx] = estimator.predict(
                np.xrange(X.shape[0])).reshape(-1, 1)

        # fit SafetyForcast and predict in the following codes.
        ###############
        # SF = SafetyForecast()
        # SF.fit(predict_values, y, labeled_idx)
        # pred = SF.predict(unlabeled_idx)
        ###############
