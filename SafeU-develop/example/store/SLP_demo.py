import numpy as np
import scipy.io as sio
import timeit
import h5py
from safeu.data_quality.SLP import SLP
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# Demo showing code performance and calling methods.

# covtype can be downloaded from http://lamda.nju.edu.cn/code_SLP.ashx

def load(load_fn, W, y, load_lb):
    """Load data.

    Parameters
    ----------
    load_fn: string, .mat file of data source
    W: key word of data matrix
    y: key word of label vector
    load_lb: string, .mat file of labeled and unlabeled indexes


    Returns
    -------
    W0: sparse matrix
    y: label vector
    idx: index

    """

    data = sio.loadmat(load_fn)
    W0 = data[W]
    y = np.squeeze(data[y])

    idx = h5py.File(load_lb, 'r')

    return W0, y, idx


def Accuracy(f, y, U):
    """calculate ACC

    Parameters
    ----------
    f: predicting vector
    y: label vector
    U: indexes vector

    Returns
    -------
    the value of acc
    """
    U = U.astype(np.int)
    count = 0
    for i in range(len(U)):
        if f[i] == y[i]:
            count += 1
    return count / U.shape[0]


# Demo showing code performance and calling methods.
if __name__ == "__main__":

    load_fn = 'data/covtype.mat'
    load_lb = 'data_ssl/covtype.mat'
    W_ = 'W'
    y_ = 'y'
    labled_ = 'L'
    unlabeled_ = 'U'

    [W0, y, idx] = load(load_fn, W_, y_, load_lb)
    slp = SLP()

    T = 20
    times = np.zeros([T, 1])
    accs = np.zeros([T, 1])
    aucs = np.zeros([T, 1])

    for t in range(T):
        L_float = np.array([idx[element[t]][:] for element in idx[labled_]])
        L = np.squeeze(L_float).astype(np.int) - 1
        U_float = np.array([idx[element[t]][:] for element in idx[unlabeled_]])
        U = np.squeeze(U_float).astype(np.int) - 1
        start = timeit.default_timer()

        slp.fit(W0, y, L)
        fs = slp.predict(U)

        times[t] = timeit.default_timer() - start
        accs[t] = Accuracy(fs, y[U], U)
        # aucs[t] = roc_auc_score(y, fs)
        print(t + 1, ":", "acc", accs[t], "time", times[t])

    print("MEAN")
    print("acc", np.mean(accs), "time", np.mean(times))
