from safeu.model_uncertainty.SAFER import SAFER
import numpy as np
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# Demo showing code performance and calling methods.

if __name__ == "__main__":
    load_fn = "housing10.mat"
    safer = SAFER()

    [ground_truth, label, label_instance,
        unlabel_instance] = safer.load(load_fn)
    label_num = label_instance.shape[0]
    unlabel_num = unlabel_instance.shape[0]

    X = safer.minmax_normalized(np.r_[label_instance, unlabel_instance])
    label_instance = X[0:label_num:1]
    unlabel_instance = X[label_num:X.shape[0]:1]

    y = safer.minmax_normalized(np.r_[label, ground_truth])
    label = y[0:label_num:1]
    ground_truth = y[label_num:y.shape[0]:1]

    prediction1 = safer.Self_KNN_predict(
        label_instance, label, unlabel_instance, 'euclidean', 3)
    prediction2 = safer.Self_KNN_predict(
        label_instance, label, unlabel_instance, 'cosine', 3)

    candidate_prediction = np.hstack((prediction1, prediction2))
    baseline_prediction = safer.Self_NN_predict(
        label_instance, label, unlabel_instance, 'euclidean', 1)

    Safer_prediction = safer.Safer_predict(
        candidate_prediction, baseline_prediction)

    mse = np.sum((Safer_prediction - ground_truth)**2) / \
                 (label_num + unlabel_num)
    print("mse", mse)
