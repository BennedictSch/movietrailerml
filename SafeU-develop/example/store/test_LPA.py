from sklearn.semi_supervised import label_propagation
from safeu.classification.LPA import LPA
import scipy.io as sio
import numpy as np
import sys
import os
import h5py
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


dataname = 'spambase'
C1 = 1
C2 = 0.01
T = 20

data2 = sio.loadmat('data/isolet,splits,labeled=10.mat')
data1 = h5py.File('data/isolet.mat')

X, y = data1['X'].value.T, data1['y'].value.T
idxUnls = data2['idxUnls'] - 1
idxLabs = data2['idxLabs'] - 1
index1 = idxLabs[0, :]
index2 = idxUnls[0].reshape((1, -1))
label_instance = X[index1, :]
label = y
unlabel_instance = np.delete(X, index1, axis=0)
ground_truth = np.delete(y, index1, axis=0)
labels = []
for i in range(0, len(X)):
    if i not in index1:
        labels.append(i)

labels = np.array(labels)

label2 = -np.ones(len(X)).reshape(-1, 1)
label2[index1] = y[index1] + 1

model = label_propagation.LabelPropagation()
model.fit(X, label2)
W = model._build_graph()
# prediction = model.predict(labels).reshape((-1, 1))

model2 = LPA()
model2.fit(X, label, index1, W)
prediction = model2.predict(labels).reshape((-1, 1))

result = np.array((prediction == ground_truth).nonzero()
                  ).shape[1] / ground_truth.shape[0]
print(result)
