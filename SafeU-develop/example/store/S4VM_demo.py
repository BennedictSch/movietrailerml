from scipy.spatial.distance import pdist, squareform
from safeu.model_uncertainty.S4VM import S4VM
import scipy.io as sio
import numpy as np
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


C1 = 100
C2 = 0.1
sample_time = 100

data2 = sio.loadmat('data/australian,splits,labeled=10.mat')
data1 = sio.loadmat('data/australian.mat')
# data1 = h5py.File('data/isolet.mat')

# X, y = data1['X'].value.T, data1['y'].value.T
X, y = data1['X'], data1['y']
idxUnls = data2['idxUnls'] - 1
idxLabs = data2['idxLabs'] - 1

dist = squareform(pdist(X))
num = dist.size
gamma = num / np.sum(dist)

times = 1
accLinear = np.zeros((times, 1))
accRBF = np.zeros((times, 1))

for j in range(0, times):
    index1 = idxLabs[j, :]
    label_instance = X[index1, :]
    label = y

    index2 = idxUnls[j].reshape((1, -1))
    unlabel_instance = X[index2][0]
    ground_truth = np.delete(y, index1, axis=0)
    u_ind = np.delete(np.arange(0, len(y)), index1)

    model = S4VM(kernel='Linear')
    # model.fit(label_instance=label_instance, label=label,
    # unlabel_instance=unlabel_instance)
    model.fit(X, label, index1)
    prediction = np.array(model.predict(u_ind)).reshape(-1, 1)
    accLinear[j] = np.array(
        (prediction == ground_truth).nonzero()).shape[1] / \
                   ground_truth.shape[0]

    # model = S4VM(kernel='RBF', C1=C1, C2=C2, sample_time=sample_time,
    #  gamma=gamma)
    # model.fit(label_instance=label_instance, label=label,
    # unlabel_instance=unlabel_instance)
    # model.fit(X, label, index1)
    # prediction = np.array(model.predict(u_ind)).reshape(-1, 1)
    # accRBF[j] = np.array((prediction == ground_truth).nonzero()).shape[1] / \
    #  ground_truth.shape[0]

print(accLinear.mean(axis=0))
print(accRBF.mean(axis=0))
