from sklearn.svm import SVC
import numpy as np

from safeu.classification import CoTraining


def get_data(name):
    features = np.loadtxt('./data/{0}/features.csv'.format((name)))
    targets = np.loadtxt('./data/{0}/targets.csv'.format((name))).astype(int)
    print(features.shape)
    print(targets.shape)
    return features, targets  # 输出为 ndarrays


if __name__ == "__main__":
    v1 = 0
    v2 = 1
    dataset = 'course_full'
    feature, targets = get_data(dataset)
    feature_views_ind = feature[0, :]  # 第0行用于指明如何进行视图分割
    nclass = int(np.max(targets).astype(int) + 1)

    ind1 = np.arange(feature.shape[1])[feature_views_ind == v1]
    ind2 = np.arange(feature.shape[1])[feature_views_ind == v2]
    nfeat1 = len(ind1)
    nfeat2 = len(ind2)

    feature1 = feature[1:, ind1]
    feature2 = feature[1:, ind2]

    m1 = SVC(kernel='linear', probability=True)

    m2 = SVC(kernel='linear', probability=True)

    labeled_ind = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1000, 1020]

    co = CoTraining(pos=1, neg=3, model1=m1, model2=m2, ind1=ind1, ind2=ind2)
    co.fit(feature[1:, :], targets, labeled_ind)
    print(co.predict(feature))

    co._fit(feature1, feature2, targets, labeled_ind)

    print(co.predict(feature))
