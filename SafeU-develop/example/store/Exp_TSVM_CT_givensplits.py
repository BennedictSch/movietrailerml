'''
This script shows how to config a SSL experiments with given
'''

from safeu.Experiments import SslExperimentsWithoutGraph
from safeu.classification.CoTraining import CoTraining
from safeu.classification.TSVM import TSVM
import numpy as np
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


if __name__ == '__main__':
    test_ratio = 0.2
    unlabel_ratio = 0.75
    number_init = 5
    all_class = True
    configs = [
        ('TSVM', TSVM(), {
            'alpha': [0.1, 0.5],
            'beta': [-1]}),
        ('CoTraining', CoTraining(), {
            'pos': [207],
            'neg': [269],
            'ind1': np.linspace(0, 29, 30, dtype=int),
            'ind2': np.linspace(80, 119, 40, dtype=int)
        })
    ]
    datasets = [
        ('clean1', None, None,
         os.path.join(os.path.dirname(__file__), 'split'), None)
    ] 
    # [(name, feature_file, label_file, split_path, graph_file)]
    # When split_path is not None, a random split will be provided.

    ##############################################
    # The scirpt to split the dataset and save to split_path
    # provided existing split file, the experiment would use the given ones
    # instead of split the dataset again.
    '''if not os.path.exists(os.path.join(os.path.dirname(__file__), 'split')):
        os.mkdir(os.path.join(os.path.dirname(__file__), 'split'))
    for name, feature_file, label_file, split_path, _ in datasets:
        X, y = base.load_dataset(name, feature_file, label_file)
        _, test_idxs, labeled_idxs, unlabeled_idxs = 
        data_manipulate.inductive_split(X=X, y=y, test_ratio=test_ratio,
          initial_label_rate=(1-test_ratio)*(1-unlabel_ratio), save_file=True,
            split_count=number_init, all_class=all_class,
             saving_path=split_path, name=name)'''
    # uncomment this part for the first time
    ##############################################

    experiments = SslExperimentsWithoutGraph(transductive=False, n_jobs=4)
    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')
    results = experiments.experiments_on_datasets(
        unlabel_ratio=unlabel_ratio, test_ratio=test_ratio,
        number_init=number_init)

    # You can now do something with the results #
