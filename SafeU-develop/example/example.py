import os

exp_list = ['Exp_CT.py',
            'Exp_LPA.py',
            'Exp_S4VM.py',
            'Exp_SAFER.py',
            'Exp_SF.py',
            'Exp_SLP.py',
            'Exp_SLP_givensplit.py',
            'Exp_TSVM.py',
            'Exp_TSVM_givensplit.py',
            'LEAD_demo.py',
            'SafertyForecast_demo.py', ]

for exp in exp_list:
    os.system("python " + exp)
