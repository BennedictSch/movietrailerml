from safeu.Experiments import SslExperimentsWithoutGraph
from safeu.model_uncertainty.S4VM import S4VM
from safeu.classification.TSVM import TSVM
from safeu.ensemble.SafetyForecast import SafetyForecast
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    estimator_list = [(TSVM(), False),
                      (S4VM(), True)]

    configs = [
        ('SafetyForecast', SafetyForecast(), {
            'C1': [1.0],
            'C2': [0.01],
            'estimators': [estimator_list]
        })
    ]

    # datasets
    datasets = [
        ('house', None, None, None, None),
        ('bupa', None, None, None, None)
    ]
    experiments = SslExperimentsWithoutGraph(transductive=True, n_jobs=4)

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=2)

    # do something on results #
