import os
import sys
import numpy as np

sys.path.append(os.path.dirname(os.path.dirname(__file__)))

from safeu.Experiments import SslExperimentsWithoutGraph
from safeu.classification.CoTraining import CoTraining


if __name__ == '__main__':

    configs = [
        ('CoTraining', CoTraining(), {
            'pos': [1],
            'neg': [1],
            'ind1': [np.linspace(0, 6, 7, dtype=int)],
            'ind2': [np.linspace(7, 15, 9, dtype=int)]
        })
    ]

    # datasets
    datasets = [
        ('house', None, None, None, None)
    ]
    experiments = SslExperimentsWithoutGraph(transductive=False, n_jobs=2)

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)

    experiments.set_metric(performance_metric='hamming_loss')
    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=10)

    experiments.set_metric(performance_metric='accuracy_score')
    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=10)

    # do something on results #
