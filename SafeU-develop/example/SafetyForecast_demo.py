from safeu.datasets.data_manipulate import check_y, modify_y
from safeu.ensemble.SafetyForecast import SafetyForecast
from sklearn.neighbors import KNeighborsClassifier
from safeu.model_uncertainty.S4VM import S4VM
from safeu.datasets import data_manipulate, base
from safeu.metrics.performance import accuracy_score
from safeu.classification.TSVM import TSVM
import numpy as np
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    unlabel_ratio = 0.9

    # datasets
    datasets = [
        ('house', None, None),
        ('isolet', None, None)
    ]

    estimator_list = [(TSVM(), False),
                      (S4VM(), True)]

    # load dataset and split
    results = dict()  # dict === {dataset_name: {config_name:[scores]} }
    for name, feature_file, label_file in datasets:
        # load dataset
        X, y = base.load_dataset(name, feature_file, label_file)

        # split
        _, test_idxs, labeled_idxs, unlabeled_idxs =\
            data_manipulate.inductive_split(X=X, y=y, test_ratio=0.,
                                            initial_label_rate=1
                                                               - unlabel_ratio,
                                            split_count=1, all_class=True)

        labeled_idx = labeled_idxs[0]
        unlabeled_idx = unlabeled_idxs[0]

        predict_values = None
        # predict with given base estimators
        for estimator, transductive in estimator_list:
            estimator.fit(X, y, labeled_idx)

            temp_res = np.zeros_like(y)
            temp_res[labeled_idx] = y[labeled_idx]
            if transductive:
                temp_res[unlabeled_idx] = estimator.predict(
                    unlabeled_idx).reshape(-1, 1)
            else:
                temp_res[unlabeled_idx] = estimator.predict(
                    X[unlabeled_idx]).reshape(-1, 1)

            if predict_values is None:
                predict_values = temp_res
            else:
                predict_values = np.hstack((predict_values, temp_res))

        # provide baseline prediction
        n_labels, label = check_y(y, binary=True)
        knn = KNeighborsClassifier()
        knn.fit(X[labeled_idx], np.array(y).reshape(1, -1)[0][labeled_idx])
        baseline_pred = y
        baseline_pred[unlabeled_idx] = knn.predict(
            X[unlabeled_idx]).reshape(-1, 1)
        baseline_pred = modify_y(baseline_pred, range(
            0, len(baseline_pred)), n_labels)

        # fit SafetyForcast and predict in the following codes.
        ###############
        SF = SafetyForecast()
        SF.fit(predict_values, y, labeled_idx)
        pred = SF.predict(unlabeled_idx, baseline_pred=baseline_pred)
        ###############

        results[name] = accuracy_score(y[unlabeled_idx], pred)

    print(results)
