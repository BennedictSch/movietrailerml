from safeu.model_uncertainty.S4VM import S4VM
from safeu.Experiments import SslExperimentsWithoutGraph
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    configs = [
        ('S4VM', S4VM(), {
            'kernel': ['rbf'],
            'gamma': [0.01],
            'C1': [50],
            'C2': [0.05],
            'sample_time':[100],
        }),

    ]

    # datasets
    # name,feature_file,label_file,split_path,graph_file
    # note that the path of file should be absolute path, for example:
    # given 'house_feature.csv' and 'house_label.csv' in current directory,
    # we should code as following:
    ###################################
    # path = dirname(__file__)
    # datasets = [
    #    (None,
    #     join(path,'house_feature.csv'),
    #     join(path,'house_label.csv'),
    #     None,
    #     None)
    # ]
    ###################################

    datasets = [
        ('house', None, None, None, None),
        ('isolet', None, None, None, None)
    ]
    experiments = SslExperimentsWithoutGraph(transductive=True, n_jobs=4)

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=2)
    

    # do something on results #
