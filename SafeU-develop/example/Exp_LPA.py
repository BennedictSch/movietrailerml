from safeu.Experiments import SslExperimentsWithGraph
from safeu.classification.LPA import LPA
import sys
import os

sys.path.append(os.path.dirname(os.path.dirname(__file__)))


# algorithm configs


if __name__ == '__main__':
    configs = [
        ('LPA', LPA(), {
            'kernel': ['rbf'],
        })
    ]
    '''
    'gamma': [20,10],
            'n_neighbors':[3,5,7],
            'alpha':['None'],
            'max_iter':[30,50],
            'tol': [1e-3]
    '''
    # datasets
    # name,feature_file,label_file,split_path,graph_file
    # note that the path of file should be absolute path, for example:
    # given 'covtype_feature.npz','covtype_label.csv' and 'covtype_W.npz'
    # in current directory, we should code as following:
    #################################
    # path = dirname(__file__)
    # datasets = [
    #    (None,
    #     join(path,'covtype_feature.npz'),
    #     join(path,'covtype_label.csv'),
    #     None,
    #     join(path,'covtype_W.npz')),
    # ]
    #################################

    '''
    
        ('covtype', None, None, None, None),
    '''
    datasets = [
        ('ionosphere', None, None, None, None)
    ]

    experiments = SslExperimentsWithGraph(n_jobs=1)

    experiments.append_configs(configs)
    experiments.append_datasets(datasets)
    experiments.set_metric(performance_metric='accuracy_score')

    results = experiments.experiments_on_datasets(
        unlabel_ratio=0.75, test_ratio=0.2, number_init=4)

    # do something on results #
