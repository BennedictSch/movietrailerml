import os
import sys
import platform
import re
import argparse
import shutil
from setuptools import setup


def clear_dist():
    if os.path.exists('dist'):
        shutil.rmtree('dist')
    if os.path.exists('safeu.egg-info'):
        shutil.rmtree('safeu.egg-info')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--upgrade", help="upgrade tools", 
                        action="store_true")
    args = parser.parse_args()

    if args.upgrade:
        os.system("python -m pip install --upgrade setuptools wheel")
        os.system("python -m pip install --user --upgrade twine")
    
    clear_dist()
    os.system("python setup.py sdist bdist_wheel")
    os.system("python -m twine upload dist/*")
    sys.exit()
