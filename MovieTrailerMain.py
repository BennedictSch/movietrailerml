import FeatureLabelExtraction as fle
import SVM
import ConstructTrailer
import multiprocessing
import tensorflow as tf
from PIL import Image
import numpy as np
import json

# Main-Datei in der die Funktionen importiert und aufgerufen werden

# Movie und trailer als Pfad (zur MP4), Optional: Anzahl Prozesse
def run_with_trailer(movie, trailer, num_processes=12): 

    scene_list_trailer = fle.extract_scenes(trailer)
    shots_trailer = fle.extract_shot_frames_trailer(trailer, scene_list_trailer)

    if __name__ == '__main__':

        manager = multiprocessing.Manager()
        found_indices = manager.list()


        scene_list_movie = fle.extract_scenes(movie)
        
        shots_movie, scene_frames = fle.extract_shot_frames_movie(movie, scene_list_movie,125)

        pool = multiprocessing.Pool(processes=num_processes)

        labels = pool.map(fle.search_trailer_in_movie,  [(keyframe, found_indices, shots_trailer) for keyframe in shots_movie])

        print(labels,len(labels),len(scene_list_movie))

        devices = tf.config.list_physical_devices('GPU')
        if len(devices)>0:
            tf.config.experimental.set_memory_growth(devices[0],True)
        
        features=[]
        base_model = tf.keras.applications.vgg16.VGG16(weights = 'imagenet', include_top = True)
    
        frame_features = []

        for i in range(len(labels)):
            img = Image.fromarray(np.uint8(shots_movie[i]))
            img= img.resize((224,224))

            x= tf.keras.preprocessing.image.img_to_array(img)
            x = x.reshape((1, x.shape[0], x.shape[1], x.shape[2]))
            x = tf.keras.applications.vgg16.preprocess_input(x)

            frame_features  = base_model.predict(x)
            features.append(frame_features.tolist())

        features = np.array(features)
        labels = np.array(labels)
        scene_frames = np.array(scene_frames)

       
        # Speichern der Features und Labels in einer Datei
        data = {'features': features.tolist(), 'labels': labels.tolist(), 'scene_frames': scene_frames.tolist()}
        filename = movie.split('.')[0]+'.json'
        with open(filename, 'w') as file:
                    json.dump(data, file)

        return filename

#########################################
### Aufrufe der verschiedenen Funktionen
#########################################
    
movie =  "Hobbit2.json" # run_with_trailer("ArmyOfDarkness.mp4","AODTrailer.mp4") #Label Generierung für Trainingsdaten
print("Start")
# movie2 = fle.run("VictorFrankenstein.mp4") 
movie2 = "VictorFrankenstein.json" 


lower_limit_trailerscenes = 10
upper_limit_trailerscenes = 20
multiplier = 5
trailer_scene_count = SVM.run(movie2, multiplier, upper_limit_trailerscenes)

count = 1
while trailer_scene_count < lower_limit_trailerscenes:
      trailer_scene_count = SVM.run(movie2, multiplier-count, upper_limit_trailerscenes)
      if count < multiplier:    
        count += 1  
      else:
           break

      
ConstructTrailer.run(movie2.split('.')[0], with_context_scenes=True) #FilmTitel ohne Dateiendung


