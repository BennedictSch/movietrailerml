import pandas as pd
import json
from sklearn.model_selection import train_test_split
from sklearn import svm
import numpy as np

# Ausgelesene Daten aus json in Pandas Dataframes umwandeln
def get_data_frames(pathlist, multiplier):

    dfFeaturesLabelsAll = pd.DataFrame()

    for path in pathlist:

        with open(path) as file:
            data = json.load(file)             

        dfTemp = pd.json_normalize(data, ['features'])
        dfTemp.columns = ['Features']
        columNames = []
        for i in range(1000):
            columNames.append('Features_' + str(i))
        dfFeatures = pd.DataFrame(dfTemp['Features'].to_list(), columns = columNames)

        dfLabels = pd.DataFrame(
            {
                "Labels": data['labels'],
                })
        dfFeatures.index.name = "id"
        dfLabels.index.name = "id"
        dfFeaturesLabels = dfLabels.merge(dfFeatures, right_on="id", left_on="id")

        valueCount = dfFeaturesLabels['Labels'].value_counts()

        #Verhältnis von -1 und 1 Einträge in einem Dataframe anpassen
        positiveOnes = dfFeaturesLabels[dfFeaturesLabels['Labels'] > 0]
        negativeOnes = dfFeaturesLabels[dfFeaturesLabels['Labels'] < 0] 
        negativeOnes = negativeOnes[(valueCount[-1]//2):(valueCount[-1]//2+valueCount[1]*multiplier)] #Einträge aus der Mitte bis Mitte + valueCount[1]
        dfFeaturesLabels = pd.concat([positiveOnes,negativeOnes], ignore_index=False)

        dfFeaturesLabelsAll = pd.concat([dfFeaturesLabelsAll, dfFeaturesLabels], ignore_index=True)
        
        
    return dfFeaturesLabelsAll 
        

def run(movie, multiplier, upper_limit_trailerscenes=35):

    pathlist = ["DuneMovie.json", "Wonderland2.json", "CodenameUncle.json", "Blood.json", "TheNiceGuys.json", "BraveheartMovie.json", "ArmyOfDarkness.json"]
    dfFeaturesLabelsAll = get_data_frames(pathlist, multiplier)

    with open(movie) as file:
                data = json.load(file)             

    print("Dataframe of features will be born into existence.")

    dfTemp = pd.json_normalize(data, ['features'])
    dfTemp.columns = ['Features']
    columNames = []
    for i in range(1000):
        columNames.append('Features_' + str(i))
    dfFeaturesPrediction = pd.DataFrame(dfTemp['Features'].to_list(), columns = columNames)

    print("Dataframe of labels will be summoned.")

    # Training und Prediction der SVM
    X = dfFeaturesLabelsAll.loc[:,dfFeaturesLabelsAll.columns != "Labels"]
    y = dfFeaturesLabelsAll["Labels"]
    X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=0.20,random_state=42)

    c = 1
    one_counts = 100
    while one_counts > upper_limit_trailerscenes:
        if(one_counts == 0 or c < 0.1):
            break      
        svc = svm.SVC(C=c, gamma="scale")
        svc.fit(X_train,y_train)
        print(svc.score(X_test,y_test))
        results = svc.predict(dfFeaturesPrediction) 
        one_counts = np.count_nonzero(results == 1)
        print(str(one_counts) + " Szenen wurden gefunden.")
        c -= 0.1          

    if(one_counts != 100 ):
        result_data = {'predictions': results.tolist()}
        filename = "resultsSVM" + movie
        with open(filename, 'w') as file:
                json.dump(result_data, file)

    return one_counts


            