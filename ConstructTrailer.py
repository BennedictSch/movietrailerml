import json
import moviepy.editor
import numpy as np
from scenedetect import SceneManager
from scenedetect import  open_video, AdaptiveDetector
import datetime
from os import startfile

def extract_scenes(scene_frames_path):
    with open(scene_frames_path, 'r') as file:
        data = json.load(file)
        scene_frames = np.array(data['scene_frames'])
    return scene_frames

# Erstellen des Trailers aus den vorgeschlagenen Szenen
def create_trailer(scene_frame_list, scene_indices, video_path, output_path):
    trailer_scene_list = []
    last_start_frame = -1
    
    for index in scene_indices:
        frame = scene_frame_list[index]
        start_frame = frame[0]
        if(last_start_frame != start_frame): #Verhindern, dass Szene doppelt hinzugefügt wird
            trailer_scene_list.append(frame)
            last_start_frame = start_frame

    in_file = moviepy.editor.VideoFileClip(video_path)
    clip_list = []
    for trailer_scene in trailer_scene_list:
        clip = in_file.subclip(trailer_scene[0]/in_file.fps, trailer_scene[1]/in_file.fps)
        clip_list.append(clip)
    final_clip = moviepy.editor.concatenate(clip_list)
    final_clip.write_videofile(output_path)

def run(title, with_context_scenes = True):

    movie = title + ".mp4"
    time_stamp = datetime.datetime.now()
    time_stamp_string = time_stamp.strftime("%d%m%Y_%H_%M")
    output = "AITrailer"+ title + "_" + str(time_stamp_string) + ".mp4"
    scene_frame_list = extract_scenes(title +".json")

    with open("resultsSVM"+title+".json", 'r') as file:
            data = json.load(file)
            predictions = np.array(data['predictions'])

    scene_indices = set()


    for i in range (len(predictions)):
        if (predictions[i] == 1):
            if with_context_scenes:
                scene_indices.add(i-1)
                scene_indices.add(i)
                scene_indices.add(i+1)
            else:
                scene_indices.add(i)

    scene_indices = list(scene_indices)
    scene_indices.sort()
    #scenes = scenes[0:len(scenes)-3]
    create_trailer(scene_frame_list, scene_indices, movie, output)
    startfile(output)


